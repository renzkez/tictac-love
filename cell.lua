--
-- class Cell
--

require('utils')

Cell = { occupier = nil }

local cross = love.graphics.newImage("img/cross.png")
local nought = love.graphics.newImage('img/nought.png')

function Cell.Create(posx, posy)
   return deepcopy(Cell)
end

function Cell:isEmpty()
    return self.occupier == nil
end

function Cell:isOccupiedBy( user )
    return self.occupier == user
end

function Cell:assignTo( user )
    if self:isEmpty() then
        self.occupier = user
    end
end

function Cell:draw(size, x, y)
    if not (self.occupier == nil) then
        love.graphics.setColor(0,0,0,255)
        if self.occupier == 1 then
            love.graphics.draw(cross, x, y, 0, size / cross:getWidth(),  size / cross:getHeight())
        elseif self.occupier==2 then
            love.graphics.draw(nought, x, y, 0, size / cross:getWidth(),  size / cross:getHeight())
        end
    end
end
