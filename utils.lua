
function mergeTable(t1, t2)
    for k, v in pairs(t2) do
        if (type(v) == "table") and (type(t1[k] or false) == "table") then
            mergeTable(t1[k], t2[k])
        else
            t1[k] = v
        end
    end
    return t1
end

function filterFunctions(table)
    if type(obj) ~= 'table' then
        return obj
    end
    if seen and seen[obj] then
        return seen[obj]
    end

    local s = seen or {}
    local res = setmetatable({}, getmetatable(obj))
    s[obj] = res
    for k, v in pairs(obj) do
        if (type(v) ~= 'function') then
            res[deepcopy(k, s)] = deepcopy(v, s)
        end
    end

    return res
end

function deepcopy(obj, seen)
    if type(obj) ~= 'table' then
        return obj
    end
    if seen and seen[obj] then
        return seen[obj]
    end

    local s = seen or {}
    local res = setmetatable({}, getmetatable(obj))
    s[obj] = res
    for k, v in pairs(obj) do
        res[deepcopy(k, s)] = deepcopy(v, s)
    end

    return res
end
