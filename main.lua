require('board')

windowSize = 0
margin = { x=0, y=0}
board = {}
humanPlayer = 1
comPlayer = 2
currentTurn = nil

function determineWindowSize()
    return math.min(
        love.graphics.getWidth(),
        love.graphics.getHeight()
    )
end

function determineMargin()
    return {
        x = (love.graphics.getWidth() - windowSize) / 2,
        y = (love.graphics.getHeight() - windowSize) / 2,
    }
end

function gameInit()
    board = Board.Create(windowSize)
    currentTurn = math.random(1,2)
end

function love.load()
    love.window.setTitle('Tic Tac Toe')
    math.randomseed(os.time())

    windowSize = determineWindowSize()
    margin = determineMargin()

    gameInit()
end

function love.update(dt)
    if (currentTurn == comPlayer) then
        local comMove = minimax(board, comPlayer)
        makeMove(comPlayer, comMove.position.x, comMove.position.y)
    end
end

function love.mousepressed(x, y, button)
    if (currentTurn == nil) then
        gameInit()
        return
    end

    x = math.floor((((x - margin.x)/(windowSize/3)+0.5)+0.5)) -- Convert mouse pos to cell position
    y = math.floor((((y - margin.y)/(windowSize/3)+0.5)+0.5))

    if (currentTurn == humanPlayer and
        x > 0 and x < 4 and
        y > 0 and y < 4) then
        makeMove(humanPlayer, x, y) -- User take turn
    end
end

function love.draw()
    love.graphics.setBackgroundColor(255,255,255)
    love.graphics.setColor(255,255,255,255)
    board:draw(margin)
end

function makeMove(user, x, y)
    if board.grid[x][y]:isEmpty() then
        board.grid[x][y]:assignTo(user)

        if board:isWonBy(user) or table.getn(board:getEmptyCells()) == 0 then
            currentTurn = nil
        elseif currentTurn == humanPlayer then
            currentTurn = comPlayer
        else
            currentTurn = humanPlayer
        end
    end
end

function minimax(newBoard, player)
    local availableCells = board:getEmptyCells()

    if newBoard:isWonBy(humanPlayer) then
        return { score = math.random(-10, -3) }
    elseif newBoard:isWonBy(comPlayer) then
        return { score = math.random(3, 10) }
    elseif table.getn(availableCells) == 0 then
        return { score = math.random(-2, 2) }
    end

    local moves = {}
    for k,pos in pairs(availableCells) do
        local move = {}
        move.position = pos

        -- set the empty spot to the current player
        newBoard.grid[pos.x][pos.y]:assignTo(player)

        if player == comPlayer then
            local result = minimax(newBoard, humanPlayer)
            move.score = result.score
        else
            local result = minimax(newBoard, comPlayer)
            move.score = result.score
        end

        newBoard.grid[pos.x][pos.y].occupier = nil
        table.insert(moves, move)
    end

    local bestMove
    if player == comPlayer then
        local bestScore = -math.huge
        for i,move in pairs(moves) do
            if move.score > bestScore then
                bestScore = move.score
                bestMove = i;
            end
        end
    else
        local bestScore = math.huge
        for i,move in pairs(moves) do
            if move.score < bestScore then
                bestScore = move.score
                bestMove = i;
            end
        end
    end

    return moves[bestMove]
end
